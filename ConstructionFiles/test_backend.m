%% this file tests the back end for the Transfer Function Plotter.
clear all
global be                                   % declaring backend variable as global makes the code easier to read (can be used in functions without being set as input argument)
be = back_end_TransferFunctionPlotter();
be = be.create_tf_from_text();

flg_skip_all_tests_except_last_one = 1; % all the tests makes it slow ... Skipping the passing ones improves this. 

if ~flg_skip_all_tests_except_last_one
%% Plots with starting default values 
plot_all_and_close()

%% modify text with valid input and plot everything
be.system_tf_as_text = '1/(s+2*a)^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(be.flg_input_TF_txt_is_valid)
plot_all_and_close()

%% modify text with wrong input, assess that the validity flag property is false and that the system is the default one. 
assert(be.flg_input_TF_txt_is_valid)
% subsequent numbers - 1
be.system_tf_as_text = '1/(s+2a)^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(~be.flg_input_TF_txt_is_valid)
plot_all_and_close()
% subsequent numbers - 2
be.system_tf_as_text = '1/(s+a2)^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(~be.flg_input_TF_txt_is_valid)
plot_all_and_close()
% evalutation errors
be.system_tf_as_text = '1/(s+a 2)^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(~be.flg_input_TF_txt_is_valid)
plot_all_and_close()
% space
be.system_tf_as_text = '1/(s+2 a)^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(~be.flg_input_TF_txt_is_valid)
plot_all_and_close()
% check if default is correctly selected
assert(tf_are_equal(be.system_tf, be.system_tf_default))

%% input correct text but improper TF
be.system_tf_as_text = 's^2';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(be.flg_input_TF_txt_is_valid)
plot_all_and_close()
end

%% input correct text but improper TF
be.system_tf_as_text = 'exp(-0.802*s)/((s+2)*(s^2+s+5*a))';
be = be.create_tf_from_text(); % update the backend to contain new system
assert(be.flg_input_TF_txt_is_valid)
plot_all_and_close()


%% input 1, as this broke the app in version 
be.system_tf_as_text = '1';
be = be.create_tf_from_text(); % update the backend to contain new system


%% print all is good if relevant
disp('All tests passed !')



function plot_all_and_close()
    % this function creates and axis object, plots everything on is sequentially, then deletes it. It should never
    % crash (i.e. assert is implicit).
    f = figure();
    ax = nexttile();
    global be 
    

    %% LD plot
    be.plot_LD_pz_plane(ax);
    be.plot_LD_nyquist(ax)
    be.plot_LD_root_locus(ax)
    be.plot_LD_bode(ax)
    
    %% TD plot
    be.plot_TD_step_response(ax);
    be.plot_TD_impl_response(ax);
    
    close(f);
end

function flg_are_equal = tf_are_equal(tf1, tf2)
    % tf_are_equal returns true if tf1 is equal to tf2. This is assessed by checking if the impulse responses of both 
    % systems are identical. 
    tf_time_vec_1 = impulse(tf1);
    tf_time_vec_2 = impulse(tf2);
    
    flg_are_equal = all(tf_time_vec_1 == tf_time_vec_2);
end
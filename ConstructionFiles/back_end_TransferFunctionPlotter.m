classdef back_end_TransferFunctionPlotter
    % back_end_TransferFunctionPlotter gathers all properties and methods necessary for the Graphical app of the
    % Transfer Function Plotter to function properly. The intent is to have back_end_TransferFunctionPlotter as unique
    % property of the app. 
    
    properties             
        system_tf_as_text               {mustBeText}                = '1/(s+5*a)' % user inputs text, stored in this property.
        system_tf                       {mustBeA(system_tf, "tf")}  = tf(1, [1 1])          % system plotted by app
        flg_input_TF_txt_is_valid       {mustBeNumericOrLogical}    = 1                     % assess if user text of TF is valid
        
        LD_plotting_objective           {mustBeMember(LD_plotting_objective, {...
                                            'Poles/Zeros', ...
                                            'Root Locus', ...
                                            'Bode Plot', ...
                                            'Nyquist', ...
                                            'Nichols'})} = 'Poles/Zeros';      % Default choice for Laplace Domain plotting objective
        TD_plotting_objective           {mustBeMember(TD_plotting_objective, {...
                                            'Step Response', ...
                                            'Impulse Response'})} = 'Step Response';    % Default choice for Time Domain plotting objective
        
        a_sld {mustBeNumeric} = 1;  % value modified by app when corresponding gui component changes
        b_sld {mustBeNumeric} = 0.5;
        c_sld {mustBeNumeric} = 0;
        
        t_end = 20                  % limit of time plot
    end
    
    properties (Constant)
        system_tf_default = tf(1, [1 1])    % default system transfer function when input text is wrong.
        gr_lw = 3;                          % graphical element: standard line width for plots
        gr_ms = 10;                         % graphical element: standard marker size for plots
        
        gr_ylim = [-5 5]                    % graphical element: y axis limit for Laplace Domain
        gr_xlim = [-7 2]                    % graphical element: x axis limit for Laplace Domain
        
        str_status_ok = 'Everything is Ok'                                                                      % this is the text displayed when the input text is Ok.
        str_status_txt_wrong = 'Please input a valid matlab expression containing only s, a, b, c as variables' % this is the text displayed when the input text is not Ok.
        str_status_improper_tf = 'Matlab cannot simulate impulse or step responses of improper (non-causal) transfer functions. Only the Laplace Domain plot is valid.'
    
        str_switch_off = 'Open Loop';       % appears on the left of the switch
        str_switch_on  = 'Unit Feedback';   % appears on the right of the switch
    end
    
    methods
        
        function obj = back_end_TransferFunctionPlotter()
            % nothing to do during constructor. 
        end
                
        function plot_LD_nyquist(obj, uiaxes)
            % plots nyquist plot on provided uiaxes (coming from matlab app) of the object system.
            
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            hold(uiaxes, 'on')
            title(uiaxes, 'Nyquist Plot')
            grid(uiaxes, 'on')
            
            [RE,IM,W] = nyquist(obj.system_tf);
            hold(uiaxes, 'on'); 
            plot(uiaxes, squeeze(RE), squeeze(IM), "LineWidth", obj.gr_lw);
            plot(uiaxes, squeeze(RE), -squeeze(IM), "LineWidth", obj.gr_lw);
            plot(uiaxes, -1, 0, 'r+', "MarkerSize", 10, "LineWidth", obj.gr_lw)
            
            xlabel(uiaxes, 'Real(H(s))');
            ylabel(uiaxes, 'Imag(H(s))');
        end
        
        function plot_LD_nichols(obj, uiaxes)
            % plots nyquist plot on provided uiaxes (coming from matlab app) of the object system.
            
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            hold(uiaxes, 'on')
            title(uiaxes, 'Nichols Plot')
            grid(uiaxes, 'on')
            
            [Mag,Phase] = nichols(obj.system_tf);
            hold(uiaxes, 'on'); 
            plot(uiaxes, squeeze(Phase), squeeze(db(Mag)), "LineWidth", obj.gr_lw);
            plot(uiaxes, -180, 0, 'r+', "MarkerSize", 10, "LineWidth", obj.gr_lw)

            grid(uiaxes, "on")
            
            ylabel(uiaxes, 'Gain(H(s))_{dB}');
            xlabel(uiaxes, 'Phase(H(s)) [degrees]');
        end

        function plot_LD_bode(obj, uiaxes)
            % plots bode plot on provided uiaxes (coming from matlab app) of the object system.
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            title(uiaxes, 'Bode Plot')
            grid(uiaxes, 'on')

            [MAG,PHASE,W] = bode(obj.system_tf);
            yyaxis(uiaxes,'left')                   % create 2 y axis and select the left one for plot legend and scale
            semilogx(uiaxes, W, db(squeeze(MAG)), "LineWidth", obj.gr_lw);
            ylabel(uiaxes,'|H(s)|_{dB}')
            yyaxis(uiaxes,'right')                  % select right axis.
            semilogx(uiaxes,W,squeeze(PHASE), "LineWidth", obj.gr_lw);

            ylabel(uiaxes,'\angle H(s) [degrees]');
            xlabel(uiaxes, 'frequency [rad/sec]');
        end
        
        function plot_LD_root_locus(obj, uiaxes)
            % plots root locus on provided uiaxes (coming from matlab app) of the object system.
            
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            
            obj.plot_LD_pz_plane(uiaxes);       % plot the poles and zeros of the open loop system. Hold is already 'on'.
            try
                [R,K] = rlocus(obj.system_tf);  % retrieve vectors to be plotted. This fails for systems with delays.
            catch ME
                ME.identifier
                nb_approx_terms = 50;
                obj.system_tf = pade(obj.system_tf, nb_approx_terms); %  we replace the system with its pade approximation
                [R,K] = rlocus(obj.system_tf);
            end
            for rootIter = 1:size(R, 1)
               plot(uiaxes, squeeze(real(R(rootIter, :))), squeeze(imag(R(rootIter, :))), "LineWidth", obj.gr_lw);
            end
            title(uiaxes, 'Root Locus');
            
            xlabel(uiaxes, 'Real(s)');
            ylabel(uiaxes, 'Imag(s)');
            % xlim and y lim are not imposed since the root locus potentially changes 'form' when modifying the system
            % parameters. 
        end
        
        function plot_LD_pz_plane(obj, uiaxes)
            % plots poles and zeros on provided uiaxes (coming from matlab app) of the object system.
            
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            % plot imaginary axis (stability limit)
            im_as = zeros(2, 50);
            im_as(2, :) = linspace(-5, 5, 50);
            plot(uiaxes, im_as(1, :), im_as(2, :), 'r--', 'LineWidth', obj.gr_lw)
            [p, z] = pzmap(obj.system_tf);
            hold(uiaxes, 'on')
            plot(uiaxes,real(p), imag(p), "LineStyle", 'None', "MarkerSize", obj.gr_ms, "LineWidth", obj.gr_lw/2, "Marker", "x", "Color", "b");
            plot(uiaxes,real(z), imag(z), "LineStyle", 'None', "MarkerSize", obj.gr_ms, "LineWidth", obj.gr_lw/2, "Marker", "o", "Color", "b");

            title(uiaxes, 'Poles and Zeros')
            grid(uiaxes, 'on')
            xlim(uiaxes, obj.gr_xlim)
            ylim(uiaxes, obj.gr_ylim)
            
            xlabel(uiaxes, 'Real(s)');
            ylabel(uiaxes, 'Imag(s)');
        end
        
        function plot_TD_step_response(obj, uiaxes)
            % plots step response on provided uiaxes (coming from matlab app) of the object system.
            
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            try 
                [y, t] = step(obj.system_tf, obj.t_end);        % this errors if tf is improper. 
                plot(uiaxes, t, y, "LineWidth", obj.gr_lw);
                title(uiaxes, 'Unit Step Response')
                grid(uiaxes, 'on')
                xlim(uiaxes, [-0.5 obj.t_end])

                xlabel(uiaxes, 'time [sec]');
                ylabel(uiaxes, 'arbitrary unit [-]');
            catch           % this is not a great approach. Normally the only thing that can make impulse crash is if the TF is improper. But this is an assumption.
                title_str = 'cannot simulate improper (non causal) step/impulse reponse';
                obj.plot_nothing(uiaxes, title_str)
            end
            
        end
        
        function plot_TD_impl_response(obj, uiaxes)
            % plots impulse response on provided uiaxes (coming from matlab app) of the object system.
            
            cla(uiaxes, 'reset');
            if ~obj.flg_input_TF_txt_is_valid
                obj.plot_nothing(uiaxes);
                return
            end
            try
                [y, t] = impulse(obj.system_tf, obj.t_end);     % this errors if tf is improper. 
                plot(uiaxes, t, y, "LineWidth", obj.gr_lw);
                title(uiaxes, 'Impulse Response')
                grid(uiaxes, 'on')
                xlim(uiaxes, [-0.5 obj.t_end])

                xlabel(uiaxes, 'time [sec]');
                ylabel(uiaxes, 'arbitrary unit [-]');
            catch               % this is not a great approach. Normally the only thing that can make impulse crash is if the TF is improper. But this is an assumption.
                title_str = 'cannot simulate improper (non causal) step/impulse reponse';
                obj.plot_nothing(uiaxes, title_str)
            end
        end
        
        function plot_nothing(obj, uiaxes, title_str)
            % This function catches exception in plotting functions and plots nothing instead.
            arguments
                obj         back_end_TransferFunctionPlotter
                uiaxes      matlab.graphics.axis.Axes
                title_str   char                                = 'Invalid input'
            end
            cla(uiaxes, 'reset');
            plot(uiaxes, 0, 0)
            grid(uiaxes, 'on')
            title(uiaxes, title_str)
        end
        
        function [obj, sys_tf] = create_tf_from_text(obj)
            % converts the text contained in obj.system_tf_as_text to a transfer function stored in obj.system_tf.
            % the transfer function is also outputted as optional argument. 
            %
            % if the text is not valid, the property obj.flg_input_TF_txt_is_valid is set to false and the default value
            % is taken for the transfer function (stored in )
            %
            % the characters a, b, c and s in the text are converted to the corresponding variable values.
            
            str_to_eval = obj.system_tf_as_text;
            char_indices_cannot_be_numbers = [];
            
            try                                             % if the user input text triggers an error, catch all exceptions by assiging the default tf to the object.
                condition_for_error =   any(isstrprop(obj.system_tf_as_text(char_indices_cannot_be_numbers + 1),'digit')) || ...
                                        any(isstrprop(obj.system_tf_as_text(char_indices_cannot_be_numbers - 1),'digit')) || ...
                                        any(isstrprop(obj.system_tf_as_text(char_indices_cannot_be_numbers - 1),'wspace')); % parameters a,b,c have to be a number surrounded by acceptable characters to prevent unwanted final values. This excudes digits and preceding white. examples: 2a with a = 4 would become 24. 2 a with a = -2 becomes 0.
                if condition_for_error
                    error('You will not get what you want. 2a can become 24 instead of 2*a which is 4.')
                end
                
            if contains(str_to_eval, 'a')
                char_indices_cannot_be_numbers = [char_indices_cannot_be_numbers strfind(str_to_eval, 'a')];
                str_to_eval = strrep(str_to_eval,'a',num2str(obj.a_sld)); 
            end
            if contains(str_to_eval, 'b')
                char_indices_cannot_be_numbers = [char_indices_cannot_be_numbers strfind(str_to_eval, 'b')];
                str_to_eval = strrep(str_to_eval,'b',num2str(obj.b_sld)); 
            end
            if contains(str_to_eval, 'c')
                char_indices_cannot_be_numbers = [char_indices_cannot_be_numbers strfind(str_to_eval, 'c')];
                str_to_eval = strrep(str_to_eval,'c',num2str(obj.c_sld)); 
            end
                
                s                   = tf('s');              % define s as the laplace variable
                sys_tf              = tf(eval(str_to_eval));    % evaluate string as matlab instruction. s is interpreted as laplace variable and parameters are already swapped for their values.
                obj.flg_input_TF_txt_is_valid = 1;          % if the string is a valid matlab statement and does not trigger the condition for error, validity flag is true.
            catch
                sys_tf = obj.system_tf_default; 
                obj.flg_input_TF_txt_is_valid = 0;
            end
            obj.system_tf = sys_tf;
        end

        function obj = close_the_loop_if_required(obj, flg_close_loop)
            if strcmp(flg_close_loop, obj.str_switch_on)
                obj.system_tf = feedback(obj.system_tf, 1);
            else
                return
            end
        end
    end % methods
    
end
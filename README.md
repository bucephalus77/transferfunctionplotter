# TransferFunctionPlotter

this repo contains the matlab app that can be used to show to students the impact of parameters on transfer functions

HOW TO USE IT

the user can write the desired transfer function as text. This text can contain s (the laplace variable) as 
well as the parameters a, b, c (to parametrically plot the system characteristics). The cursors on the right allow to 
change the values of the parameters a, b, c. 
In case the user enters a wrong input, a status lamp becomes red and a message is displayed to inform the user. 
Some buttons are available to choose which kind of plot to show. 

HOW IT WORKS

The gui exploits a singleton object which constitutes its 'backend'. This ensures that the code used can be tracked in 
git and tested separately. 

HOW TO INSTALL IT

see dedicated document: install_guide.pdf
Or: 
In Matlab, open the file with extension mlappinstall. A window opens to install the app and create a 
shortcut to the APPS matlab bar tab. Go to the APPS section of the Matlab Bar (top of screen) and open it. 

GIT VERSIONING NOTE

Students have access to this repository containing only the function plotter and its contruction files. This repo is a 
git submodule of the general controle theorie wpo repo. 